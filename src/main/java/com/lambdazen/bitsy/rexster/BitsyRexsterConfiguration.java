package com.lambdazen.bitsy.rexster;

import java.nio.file.Paths;
import org.apache.commons.configuration.Configuration;

import com.lambdazen.bitsy.BitsyGraph;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.rexster.Tokens;
import com.tinkerpop.rexster.config.GraphConfiguration;
import com.tinkerpop.rexster.config.GraphConfigurationException;

public class BitsyRexsterConfiguration implements GraphConfiguration {
	public static final String ALLOW_FULL_GRAPH_SCANS = "allow-full-graph-scans";
	public static final String TX_LOG_THRESHOLD = "tx-log-threshold";
	public static final String REORG_FACTOR = "reorg-factor";
	
	public Graph configureGraphInstance(Configuration properties) throws GraphConfigurationException {
		String graphFile = properties.getString(Tokens.REXSTER_GRAPH_LOCATION, null);
		boolean allowFullGraphScans = properties.getBoolean(ALLOW_FULL_GRAPH_SCANS, true);
		long txLogThreshold = properties.getLong(TX_LOG_THRESHOLD, BitsyGraph.DEFAULT_TX_LOG_THRESHOLD);
		double reorgFactor = properties.getDouble(REORG_FACTOR, BitsyGraph.DEFAULT_REORG_FACTOR);
		
        if (graphFile == null || graphFile.length() == 0) {
            throw new GraphConfigurationException("Check graph configuration. Missing or empty configuration element: " + Tokens.REXSTER_GRAPH_LOCATION);
        }
        
        return new BitsyGraph(Paths.get(graphFile), allowFullGraphScans, txLogThreshold, reorgFactor);
    }
}
